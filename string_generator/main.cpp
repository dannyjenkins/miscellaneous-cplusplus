#include <iostream>
#include <stdlib.h>

#include "StringIncrementer.h"

int main(int argc, char* argv[]) {

  if (argc != 3) {
    std::cout << "Please provide a length and a character set as arguments." << std::endl;
    std::cout << "Example: ./a.out 5 abc" << std::endl;
    return 1;
  }

  int length = atoi(argv[1]);
  std::string input = argv[2];

  StringIncrementer si = StringIncrementer(length, input);
  
	si.fullCycle();
	
  return 0;
}