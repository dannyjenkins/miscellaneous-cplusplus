#ifndef STRINGINCREMENTER_H
#define STRINGINCREMENTER_H

#include <iostream>
#include <math.h>

class StringIncrementer {

private:

  /* The length of the strings to be generated and output. */
  int length;

  /* The character set to be used. */
  std::string characters;

  /*
   * The array of number values being used for conversion into a
   * string.
   * {0,0,0} -> "aaa" when characters = "abc".
   * {0,0,2} -> "aac" when characters = "abc".
   * {2,2,2} -> "ccc" when characters = "abc".
   */
  int *currentNumValues;

  /*
   * The string which is produced after currentNumValues has been
   * converted.
   */
  std::string currentValue;

  int currentIteration = 0;

  int maxIteration;

public:

  StringIncrementer(int len, std::string charSet) {
    length = len;
    characters = charSet;
    currentNumValues = new int[length];

    for (int i = 0; i < length; i++) {
      currentValue.append(std::string(1, charSet.at(currentNumValues[i])));
    }

    maxIteration = (pow(charSet.length(), length)) - 1;
  }

  int         getLength()         { return length; }
  std::string getCharSet()        { return characters; }
  int         getNumValue(int at) { return currentNumValues[at]; }
  int         getCharSetLength()  { return characters.length(); }
  int         getMaxIteration()   { return maxIteration; }
  std::string getCurrentValue()   { return currentValue; }

  void setNumValue(int at, int newValue) {
    currentNumValues[at] = newValue;
  }

  /*
   * Convert currentNumValues into its corresponding string using the
   * character set and iteration number, and store the result in
   * currentValue.
   */
  void updateCurrentValue() {
    for (int i = 0; i < getLength(); i++) {
      currentValue[i] = getCharSet().at(getNumValue(i));
    }
  }

  /*
   * Calculate the number values in the given iteration, and update
   * currentNumValues accordingly.
   *
   * when length = 3 and characters = "abc":
   * setValues(0)  -> {0,0,0}
   * setValues(2)  -> {0,0,2}
   * setValues(3)  -> {0,1,0}
   * setValues(9)  -> {1,0,0}
   * setValues(26) -> {2,2,2}
   */
  void setValues(int iteration) {
    currentIteration = iteration;
    for (int i = getLength() - 1; i >= 0; i--) {
      setNumValue(i,
        currentIteration /
        (int) pow(getCharSetLength(), (getLength() - 1) - i) %
        getCharSetLength()
      );
    }
    updateCurrentValue();
  }

  /*
   * Calculate and print each string iteration.
   */
  void fullCycle() {
    for (int i = 0; i <= getMaxIteration(); i++) {
      setValues(i);
      std::cout << getCurrentValue() << std::endl;
    }
  }
};

#endif