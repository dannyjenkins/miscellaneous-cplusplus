#include <cmath>
#include <iostream>
#include <complex>
#include <math.h>


/* Iteration formula. */
std::complex<double> mandelbrot(
                                std::complex<double> z,
                                std::complex<double> c
                                ) {
  return pow(z, 2) + c;
}

/* Calculate the absolute value of a complex number. */
double absolute_value(std::complex<double> input) {
  return sqrt((pow(input.real(), 2)) + (pow(input.imag(), 2)));
}

/*
 * Return whether or not the input complex number may be in the
 * Mandelbrot set.
 */
bool may_be_in_set(std::complex<double> input) {
  return absolute_value(input) < 2;
}

int main() {

  /* The number of iterations in each calculation. */
  int iterations = 2000;

  /*
   * The resolution of the resulting output.  A given value x will
   * result in an output of vertical resolution 2x + 1 when using the
   * default border values, i.e. displaying the entire Mandelbrot set.
   */
  int res = 80;

  int top_border    = -res;
  int bottom_border = res + 1;
  int left_border   = -(res * 2);
  int right_border  = res + 1;

  /* Each line */
  for (int line = top_border; line < bottom_border; line++) {

    /* Each block in the line */
    for (int block = left_border; block < right_border; block++) {
      double imag_part = line;
      double real_part = block;

      std::complex<double> z(0.0, 0.0);
      std::complex<double> c((real_part / res), (imag_part / res));

      for (int iter = 0; iter < iterations; iter++) {
        z = mandelbrot(z, c);
        if (!may_be_in_set(z)) break;
      }

      std::cout << (
                    may_be_in_set(z)
                    ? "\x1b[6;30;47m  \x1b[0m"
                    : "  "
                    );

    }
    std::cout << std::endl;
  }
  return 0;
}