#include <iostream>
#include <cmath>

int main(){
    double one, two;
    std::cout << "First cathetus:  ";
    std::cin  >> one;
    std::cout << "Second cathetus: ";
    std::cin  >> two;

    double hypothenuse = (sqrt((pow(one, 2)) + (pow(two, 2))));
    std::cout << "Hypothenuse:     ";
    std::cout << hypothenuse << std::endl;

    double surface_area = (one * two) / 2;
    std::cout << "Surface Area:    ";
    std::cout << surface_area << std::endl;
}
