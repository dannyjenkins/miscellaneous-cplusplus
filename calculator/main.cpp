#include <iostream>

int main() {
  std::cout << "What kind of an operation would you like to do?\n\n";
  std::cout << "1. Addition\n2. Subtraction\n3. Multiplication\n4. Division\n";

	int choice;
  std::cin >> choice;

	double num1, num2;

  std::cout << "Enter the first number:";
  std::cin  >> num1;
  std::cout << "Enter the second number:";
  std::cin  >> num2;

	if      (choice == 1) std::cout << num1 + num2 << std::endl;
	else if (choice == 2) std::cout << num1 - num2 << std::endl;
	else if (choice == 3) std::cout << num1 * num2 << std::endl;
	else if (choice == 4) {
		
		while (num2 == 0) {
      std::cout << "The dividend cannot be zero. Try again: ";
      std::cin  >> num2;
		}

		if (num1 == num2) std::cout << "1"         << std::endl;
		else              std::cout << num1 / num2 << std::endl;
	}
}
