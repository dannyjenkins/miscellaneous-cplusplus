#include <iostream>
#include <cmath>

void length(int x, int y){
    /*
    a function that takes two integers as arguments, uses these as
    coordinates and checks if the distance between the origin in the
    x-y plane and the point that these two coordinates form (in other
    words, the hypothenuse) is also an integer.
    */
    double z = sqrt(pow(x, 2) + pow(y, 2)); // the hypothenuse formula
    int intz = (int)z; // converts the double z into an integer
    if (z - intz == 0) {
        /*
        compares the double z and the integer z; if the difference is
        equal to zero, then this means that double z is a whole number,
        and vice versa, if it's not equal to zero, then double z is not
        a whole number.
        */
      std::cout << "(" << x << ", " << y << ") = " << z << ".";
      std::cout << " Integer." << std::endl;
    }
    else {
      std::cout << "(" << x << ", " << y << ") = " << z << ".";
      std::cout << " Fractional number." << std::endl;

    }
}

int main() {

	int xvalue, yvalue, xlimit, ylimit;
    xvalue = 0;
    yvalue = 0;

    std::cout << "Welcome to the bulk hypothenuse calculator." << std::endl;
    std::cout << "This program calculates every possible hypothenuse " << std::endl;
    std::cout << "from two integer catheti within a limit that you ";
    std::cout << "specify." << std::endl << "These will be given in this form:";
    std::cout << std::endl << std::endl << "(cathetus x, cathetus y) = ";
    std::cout << "hypothenuse z." << std::endl << std::endl;
    std::cout << "What should the limit on the x-axis be? ";
    std::cin  >> xlimit;
    std::cout << "What should the limit on the y-axis be? ";
    std::cin  >> ylimit;
    std::cout << std::endl;

    while (xvalue <= xlimit + 1 && yvalue <= ylimit) {
        if (xvalue == xlimit + 1) {
            if (yvalue == ylimit) {
                break;
            }
            yvalue++;
            xvalue = 0;
        }
        length(xvalue, yvalue);
        xvalue++;
    }

}
