#include <iostream>

int main() {

  int base = 1;

  while (base < 1000000) {

    if (base % 5 == 0 && base % 3 == 0)
      std::cout << "FizzBuzz\n";

    else if (base % 3 == 0)
      std::cout << "Fizz\n";

    else if (base % 5 == 0)
      std::cout << "Buzz\n";

    else
      std::cout << base << std::endl;
    base++;
  }
}
